enum SongNoteType 
{
  Pressed           = 5432,
  Released          = 5433,
  PressedByPlayer   = 5434,
  ReleasedByPlayer  = 5435
}
