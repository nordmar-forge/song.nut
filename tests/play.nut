### SAMPLEFONT REGION

acousticSampleFont <- Song.SampleFont("ACOUSTIC")
acousticSampleFont.looping = true
acousticSampleFont.fadeEnd = true
if (CLIENT_SIDE) 
{
  acousticSampleFont.bindList[KEY_Q] = "A2A3"
  acousticSampleFont.bindList[KEY_A] = "A3"
  acousticSampleFont.bindList[KEY_W] = "B2"
  acousticSampleFont.bindList[KEY_S] = "B3"
  acousticSampleFont.bindList[KEY_D] = "B3B4"
  acousticSampleFont.bindList[KEY_X] = "B4"
  acousticSampleFont.bindList[KEY_E] = "C#3"
  acousticSampleFont.bindList[KEY_F] = "C#3C#4"
  acousticSampleFont.bindList[KEY_C] = "C#4"
  acousticSampleFont.bindList[KEY_G] = "D3"
  acousticSampleFont.bindList[KEY_B] = "D5"
  acousticSampleFont.bindList[KEY_V] = "D#4"
  acousticSampleFont.bindList[KEY_R] = "E2"
  acousticSampleFont.bindList[KEY_N] = "E4"
  acousticSampleFont.bindList[KEY_T] = "F2"
  acousticSampleFont.bindList[KEY_H] = "F2F3"
}

### SAMPLEFONT REGION END



### KEY EVENTS REGION

if (CLIENT_SIDE) {

  addEvent("onKeyUp")
  addEvent("onKeyDown")

  local _presssedKeys = {}

  addEventHandler("onInit", function() {
    disableMusicSystem(false)
    enableEvent_Render(true)
  })

  addEventHandler("onKey", function(key) {
      if (!(key in _presssedKeys)) {
          _presssedKeys[key] <- true

          callEvent("onKeyDown", key)
      }
  })

  addEventHandler("onRender", function () {
      foreach (k,v in _presssedKeys) {
          if (!isKeyPressed(k)) {
              delete _presssedKeys[k]

              callEvent("onKeyUp", k)
          }
      }
  })

}

### KEY EVENTS REGION END



### KEY PLAYING REGION

if (CLIENT_SIDE) {

  addEventHandler("onKeyUp", function(key) {
      stopSampleKeyByPlayer(heroId, acousticSampleFont, key)
  })

  addEventHandler("onKeyDown", function(key) {
      playSampleKeyByPlayer(heroId, acousticSampleFont, key)
  })

}

### KEY PLAYING REGION END
