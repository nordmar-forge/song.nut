enum SongLifeStage
{
  Initialize,
  Running,
  Removing
}

class Song.Sound3d extends Sound3d
{
# private:
  _vob = null
  _targetPlayer = null

# public:
  state = SongLifeStage.Initialize
  fadeDelay = 0.04

  constructor(fileName)
  {
    base.constructor(fileName)
  }

  function setPosition(x, y, z)
  {
    if (_targetPlayer)
      return

    if (!_vob)
    {
      local vob = Vob("NOTHING")
      vob.addToWorld()
      setTargetVob(vob)
    }

    _vob.setPosition(x, y, z)
  }

  function getPosition()
  {
    if (_targetPlayer || _vob == null)
      return null

    return _vob.getPosition()
  }

  function setTargetVob(vob)
  {
    _vob = vob
    base.setTargetVob(_vob)
  }

  function setTargetPlayer(pid)
  {
    _targetPlayer = pid
    base.setTargetPlayer(_targetPlayer)
  }

  function getTargetPlayer()
  {
    return _targetPlayer
  }

  function play()
  {
    state = SongLifeStage.Running
    base.play()
  }

  function dispose(removeVob = true)
  {
    local vob = _vob
    # setTargetVob(null)

    if (removeVob && _targetPlayer == null)
      vob.removeFromWorld()

    state = SongLifeStage.Removing
  }
}

