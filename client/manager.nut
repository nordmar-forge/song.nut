class Song.Manager extends Song.SharedManager
{
# private:
  static _sounds = []

# protected:
  static function server_packet(packet) {packetRead(packet)}

  static function render() 
  {
    local dt = getTickCount();
    
    for (local i = 0; i < _sounds.len(); i++)
    {
      local snd = _sounds[i]

      if (snd.state == SongLifeStage.Running)
      {
        if (!snd.isPlaying())
        {
          snd.dispose()
          _sounds.remove(i)
        }
      }
      else if (snd.state == SongLifeStage.Removing)
      {
        # FIXME: Need make some better way for easing decrease
        # sound volume and, maybe, deletion method.
        if (snd.volume <= 0.0 || !snd.isPlaying())
        {
          snd.dispose()
          _sounds.remove(i)
        }
        snd.volume -= snd.fadeDelay
      }
    }
  }

  static function _getFileName(fontType, note)
  {
    return fontType + "_" + note + ".WAV"
  }

  static function _playSound(x, y, z, font, note)
  {
    local snd = Song.Sound3d(_getFileName(font.type, note))
    snd.setPosition(x, y, z)
    snd.looping = font.looping
    snd.fadeDelay = font.fadeDelay
    _sounds.push(snd)
    snd.play()

    log("play sound from _playSound")
  }

  static function _playSoundByPlayer(pid, font, note)
  {
    local snd = Song.Sound3d(_getFileName(font.type, note))
    snd.setTargetPlayer(pid)
    _sounds.push(snd)
    snd.play()

    log("play sound from _playSoundByPlayer")
  }

  static function _stopSound(x, y, z, font, note)
  {
    foreach (snd in _sounds)
    {
      local sndPos = snd.getPosition()
      if (sndPos != null && 
        (snd.file == _getFileName(font.type, note) && 
        x == sndPos.x && y == sndPos.y && z == sndPos.z))
      {
        if (!font.fadeEnd)
          return
        snd.state = SongLifeStage.Removing
        log("stoping sound from _stopSound")
      }
    }
  }

  static function _stopSoundByPlayer(pid, font, note)
  {
    foreach (snd in _sounds)
    {
      if (snd.file == _getFileName(font.type, note) && pid == snd.getTargetPlayer())
      {
        if (!font.fadeEnd)
          return
        snd.state = SongLifeStage.Removing
        log("stoping sound '" + snd.file + "' from _stopSoundByPlayer")
        log(_getFileName(font.type, note))
      }
    }
  }

  static function _sendPacket(x, y, z, noteType, netId, note) 
  {
    local packet = Packet()
    packet.writeUInt16(noteType)
    packet.writeInt8(netId)
    packet.writeString(note)
    packet.writeFloat(x)
    packet.writeFloat(y)
    packet.writeFloat(z)
    packet.send(RELIABLE_ORDERED)

    log("send net packet '" + noteType + "' from _sendPacket")
  }

  static function _sendPacketByPlayer(pid, noteType, netId, note) 
  {
    local packet = Packet()
    packet.writeUInt16(noteType)
    packet.writeInt8(netId)
    packet.writeString(note)
    packet.writeUInt8(pid)
    packet.send(RELIABLE_ORDERED)

    log("send net packet '" + noteType + "' from _sendPacketByPlayer")
  }

# public: 
  static function playSampleNote(x, y, z, font, note)
  {
    _sendPacket(x, y, z, SongNoteType.Pressed, font.netId, note)
  }

  static function playSampleKey(x, y, z, font, key)
  {
    local note = font.bindList[key]

    if (note == null)
      return

    playSampleNote(x, y, z, font, note)
  }

  static function playSampleNoteByPlayer(pid, font, note)
  {
    _sendPacketByPlayer(pid, SongNoteType.PressedByPlayer, font.netId, note)
  }

  static function playSampleKeyByPlayer(pid, font, key)
  {
    local note = font.bindList[key]

    if (note == null)
      return

    playSampleNoteByPlayer(pid, font, note)
  }

  static function stopSampleNote(x, y, z, font, note)
  {
    if (!font.fadeEnd)
      return
    _sendPacket(x, y, z, SongNoteType.Released, font.netId, note)
  }

  static function stopSampleKey(x, y, z, font, key)
  {
    local note = font.bindList[key]

    if (note == null)
      return

    stopSampleNote(x, y, z, font, note)
  }

  static function stopSampleNoteByPlayer(pid, font, note)
  {
    if (!font.fadeEnd)
      return
    _sendPacketByPlayer(pid, SongNoteType.ReleasedByPlayer, font.netId, note)
  }

  static function stopSampleKeyByPlayer(pid, font, key)
  {
    local note = font.bindList[key]

    if (note == null)
      return

    stopSampleNoteByPlayer(pid, font, note)
  }

  static function packetRead(packet) 
  {
    local packetId = packet.readUInt16()
    local netId = packet.readInt8()
    local note = packet.readString()

    log("'" + packetId + "' packet reading...")

    switch(packetId)
    {
      case SongNoteType.Pressed: 
        local x = packet.readFloat()
        local y = packet.readFloat()
        local z = packet.readFloat()
        _playSound(x, y, z, getSampleFontById(netId), note)
        break
      case SongNoteType.Released:
        local x = packet.readFloat()
        local y = packet.readFloat()
        local z = packet.readFloat()
        _stopSound(x, y, z, getSampleFontById(netId), note)
        break
      case SongNoteType.PressedByPlayer:
        local pid = packet.readUInt8()
        _playSoundByPlayer(pid, getSampleFontById(netId), note)
        break
      case SongNoteType.ReleasedByPlayer:
        local pid = packet.readUInt8()
        _stopSoundByPlayer(pid, getSampleFontById(netId), note)
        break
    }
  }
}

getSongManager <- @() Song.Manager 

# Song API
playSampleNote <- @(x, y, z, font, note) Song.Manager.playSampleNote(x, y, z, font, note)
playSampleKey <- @(x, y, z, font, key) Song.Manager.playSampleKey(x, y, z, font, key)
playSampleNoteByPlayer <- @(pid, font, note) Song.Manager.playSampleNoteByPlayer(pid, font, note)
playSampleKeyByPlayer <- @(pid, font, key) Song.Manager.playSampleKeyByPlayer(pid, font, key)

stopSampleNote <- @(x, y, z, font, note) Song.Manager.stopSampleNote(x, y, z, font, note)
stopSampleKey <- @(x, y, z, font, key) Song.Manager.stopSampleKey(x, y, z, font, key)
stopSampleNoteByPlayer <- @(pid, font, note) Song.Manager.stopSampleNoteByPlayer(pid, font, note)
stopSampleKeyByPlayer <- @(pid, font, key) Song.Manager.stopSampleKeyByPlayer(pid, font, key)

