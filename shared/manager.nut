class Song.SharedManager 
{
  static samplefonts = []
  static logEnabled = Song.Var(false)

  static function registerSampleFont(font) 
  {
    samplefonts.push(font)
    font.netId = samplefonts.len() - 1
  }

  static function getSampleFontById(fid)
  {
    if (fid < 0 || fid >= samplefonts.len())
      return null

    return samplefonts[fid]
  }

  static function setLogging(state)
  {
    logEnabled.value = state
  }

  static function log(msg)
  {
    if (!logEnabled.value)
      return

    local side = "[CLIENT]"
    if (SERVER_SIDE)
      side = "[SERVER]"

    print("[Song.Manager]" + side + ": " + msg)
  }
}

getSongManager <- @() Song.SharedManager
