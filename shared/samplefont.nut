class Song.SampleFont 
{
  netId = -1

  type = "DEFAULT"
  looping = false
  fadeEnd = false
  fadeDelay = 0.04

  bindList = array(128)

  constructor(type = "DEFAULT", looping = false, fadeEnd = false, fadeDelay = 0.04, bindList = array(128)) 
  {
    this.type = type
    this.looping = looping 
    this.fadeEnd = fadeEnd
    this.fadeDelay = fadeDelay
    this.bindList = bindList
    # yeah, we call manager from client/server side...
    getSongManager().registerSampleFont(this)
  }
}
