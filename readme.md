# song.nut
Song tracker manager for playing music like: .mid files streamer and some lute/flute systems.

# How to use
First, you should put `sfx` sounds into filesystem (.vdf/.mod).

It must be looks like: `<SAMPLE_TYPE>_<NOTE>.WAV`

And then, also, you need create a new `Song.SampleFont` object and use it for playing notes.

# API
**playSampleNote** `(x: float, y: float, z: float, sample_font: Song.SampleFont, note: string)` - Play sfx sound with position

**playSampleKey** `(x: float, y: float, z: float, sample_font: Song.SampleFont, key: int)` - Play sfx sound with position and keyboard button

**playSampleNoteByPlayer** `(player_id: int, sample_font: Song.SampleFont, note: string)` - Play sfx sound from player 

**playSampleKeyByPlayer** `(player_id: int, sample_font: Song.SampleFont, key: int)` - Play sfx sound from player and with keyboard button

**stopSampleNote** `(x: float, y: float, z: float, sample_font: Song.SampleFont, note: string)` - Stop sfx sound from position

**stopSampleKey** `(x: float, y: float, z: float, sample_font: Song.SampleFont, key: int)` - Stop sfx sound from position with pressed keyboard button

**stopSampleNoteByPlayer** `(player_id: int, sample_font: Song.SampleFont, note: string)` - Stop sfx sound from player

**stopSampleKeyByPlayer** `(player_id: int, sample_font: Song.SampleFont, key: int)` - Stop sfx sound from player with pressed keyboard button


# Song.SampleFont API
**type**: `string` - String type of sample file, for using to find sfx sounds (like "ACOUSTIC_A3.WAV"). 
Default value is `"DEFAULT"`

**looping**: `boolean` - Using for looping sounds, until it stoped. Default value is `false`

**fadeEnd**: `boolean` - Using for predirect stoping sound with `stopSampleKey`, `stopSampleKeyByPlayer`. Default value is `false`

**fadeDelay**: `float` - Volume fade decrease, until it stop sound. Default value is `0.04`

**bindList**: `array[128]` - Contains all keyboard bindings for playing sounds from `playSampleKey`, `playSampleKeyByPlayer` Default value is `array(128)`

# Examples
**Creating Song.SampleFont:**
```js
acousticSampleFont <- Song.SampleFont("ACOUSTIC")
acousticSampleFont.looping = true
acousticSampleFont.fadeEnd = true
if (CLIENT_SIDE) 
{
  acousticSampleFont.bindList[KEY_Q] = "A2A3"
  acousticSampleFont.bindList[KEY_A] = "A3"
  acousticSampleFont.bindList[KEY_W] = "B2"
  acousticSampleFont.bindList[KEY_S] = "B3"
  acousticSampleFont.bindList[KEY_D] = "B3B4"
  acousticSampleFont.bindList[KEY_X] = "B4"
  acousticSampleFont.bindList[KEY_E] = "C#3"
  acousticSampleFont.bindList[KEY_F] = "C#3C#4"
  acousticSampleFont.bindList[KEY_C] = "C#4"
  acousticSampleFont.bindList[KEY_G] = "D3"
  acousticSampleFont.bindList[KEY_B] = "D5"
  acousticSampleFont.bindList[KEY_V] = "D#4"
  acousticSampleFont.bindList[KEY_R] = "E2"
  acousticSampleFont.bindList[KEY_N] = "E4"
  acousticSampleFont.bindList[KEY_T] = "F2"
  acousticSampleFont.bindList[KEY_H] = "F2F3"
}
```

**Using samplefont and play notes (from player):**
```js
addEventHandler("onKeyUp", function(key) {
    stopSampleKeyByPlayer(heroId, acousticSampleFont, key)
})

addEventHandler("onKeyDown", function(key) {
    playSampleKeyByPlayer(heroId, acousticSampleFont, key)
})
```
