class Song.Manager extends Song.SharedManager
{
  static function client_packet(pid, packet) {packetRead(pid, packet)}

  static function _sendPacket(x, y, z, noteType, netId, note) 
  {
    local packet = Packet()
    packet.writeUInt16(noteType)
    packet.writeInt8(netId)
    packet.writeString(note)
    packet.writeFloat(x)
    packet.writeFloat(y)
    packet.writeFloat(z)
    packet.sendToAll(RELIABLE_ORDERED)

    log("send net packet '" + noteType + "' from _sendPacket")
  }

  static function _sendPacketByPlayer(pid, noteType, netId, note) 
  {
    local packet = Packet()
    packet.writeUInt16(noteType)
    packet.writeInt8(netId)
    packet.writeString(note)
    packet.writeUInt8(pid)
    packet.sendToAll(RELIABLE_ORDERED)

    log("send net packet '" + noteType + "' from _sendPacketByPlayer")
  }

  static function playSampleNote(x, y, z, font, note)
  {
    _sendPacket(x, y, z, SongNoteType.Pressed, font.netId, note)
  }

  static function playSampleNoteByPlayer(pid, font, note)
  {
    _sendPacketByPlayer(pid, SongNoteType.PressedByPlayer, font.netId, note)
  }

  static function stopSampleNote(x, y, z, font, note)
  {
    if (!font.fadeEnd)
      return
    _sendPacket(x, y, z, SongNoteType.Released, font.netId, note)
  }

  static function stopSampleNoteByPlayer(pid, font, note)
  {
    if (!font.fadeEnd)
      return
    _sendPacketByPlayer(pid, SongNoteType.ReleasedByPlayer, font.netId, note)
  }

  static function packetRead(pid, packet) 
  {
    local packetId = packet.readUInt16()
    local netId = packet.readInt8()
    local note = packet.readString()

    log("'" + packetId + "' packet reading...")

    switch(packetId)
    {
      case SongNoteType.Pressed: 
        local x = packet.readFloat()
        local y = packet.readFloat()
        local z = packet.readFloat()
        playSampleNote(x, y, z, getSampleFontById(netId), note)
        break
      case SongNoteType.Released:
        local x = packet.readFloat()
        local y = packet.readFloat()
        local z = packet.readFloat()
        stopSampleNote(x, y, z, getSampleFontById(netId), note)
        break
      case SongNoteType.PressedByPlayer:
        local plr = packet.readUInt8() # why i read player id... 
        playSampleNoteByPlayer(pid, getSampleFontById(netId), note)
        break
      case SongNoteType.ReleasedByPlayer:
        local plr = packet.readUInt8() # real lol...
        stopSampleNoteByPlayer(pid, getSampleFontById(netId), note)
        break
    }
  }
}

getSongManager <- @() Song.Manager 

# Song API
playSampleNote <- @(x, y, z, font, note) Song.Manager.playSampleNote(x, y, z, font, note)
playSampleNoteByPlayer <- @(pid, font, note) Song.Manager.playSampleNoteByPlayer(pid, font, note)

stopSampleNote <- @(x, y, z, font, note) Song.Manager.stopSampleNote(x, y, z, font, note)
stopSampleNoteByPlayer <- @(pid, font, note) Song.Manager.stopSampleNoteByPlayer(pid, font, note)
